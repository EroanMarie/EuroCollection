package com.example.eroan.eurocollection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Eroan on 01/02/2018.
 */

public class CommemoActivity extends AppCompatActivity{

    String[] annees = {"2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018"};

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commemo);
        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), R.layout.commemolayout, R.id.textViewCommemo, annees);
        ListView mListView = (ListView) findViewById(R.id.mListView);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = annees[+position];
                Intent intent = new Intent(CommemoActivity.this,CompteActivity.class);
                intent.putExtra("From","Commemo");
                intent.putExtra("Choix",selectedItem);
                startActivity(intent);
            }
        });
    }
}
