package com.example.eroan.eurocollection;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by Eroan on 11/01/2018.
 */

public class CompteActivity extends AppCompatActivity {

    String[] valeurs = {"1 Centime","2 Centimes","5 Centimes","10 Centimes","20 Centimes","50 Centimes","1 Euro","2 Euros"};
    JSONArray datas = null;
    /*String[] commemo = {"Finlande","Grèce","Italie","Luxembourg","Saint-Marin","Vatican",
    "Autriche","Belgique","Espagne","Finlande","Italie","Luxembourg","Saint-Marin","Vatican",
    "Allemagne","Belgique","Finlande","Italie","Luxembourg","Saint-Marin","Vatican",
    "Allemagne","TraitéDeRome","Finlande","Luxembourg","Monaco","Portugal","Saint-Marin","Vatican",
    "Allemagne","Belgique","Finlande","France","Italie","Luxembourg","Portugal","Saint-Marin",
    "Slovénie","Vatican",
    "Allemagne","Belgique","Finlande","Italie","Luxembourg","Portugal","Saint-Marin","Slovaquie",
    "Vatican","DixAnsUnionÉconomiqueEtMonétaire",
    "Allemagne","Belgique","Espagne","Finlande","France","Grèce","Italie","Luxembourg","Portugal",
    "Saint-Marin","Slovénie","Vatican",
    "Allemagne","Belgique","Espagne","Finlande","France","Grèce","Italie","Luxembourg",
    "Malte","Monaco","Pays-Bas","Portugal","Saint-Marin","Slovaquie","Slovénie","Vatican",
    "Allemagne","Belgique","Espagne","Finlande","France","Italie","Luxembourg","GrandsDucsLuxembourg",
    "Monaco","Portugal","Vatican","DixHuitsPaysZoneEuro",

    };*/
    //2004
    String[] m2004 = {"Finlande","Grèce","Italie","Luxembourg","Saint-Marin","Vatican"};
    //2005
    String[] m2005 = {"Autriche","Belgique","Espagne","Finlande","Italie","Luxembourg","Saint-Marin","Vatican"};
    //2006
    String[] m2006 = {"Allemagne","Belgique","Finlande","Italie","Luxembourg","Saint-Marin","Vatican"};
    //2007
    String[] m2007 = {"Allemagne","TraitéDeRome","Finlande","Luxembourg","Monaco","Portugal","Saint-Marin","Vatican"};
    //2008
    String[] m2008 = {"Allemagne","Belgique","Finlande","France","Italie","Luxembourg","Portugal","Saint-Marin",
    "Slovénie","Vatican"};
    //2009
    String[] m2009 = {"Allemagne","Belgique","Finlande","Italie","Luxembourg","Portugal","Saint-Marin","Slovaquie",
    "Vatican","ZoneEuro"};
    //2010
    String[] m2010 = {"Allemagne","Belgique","Espagne","Finlande","France(18Juin)","Grèce","Italie","Luxembourg","Portugal",
    "Saint-Marin","Slovénie","Vatican"};
    //2011
    String[] m2011 = {"Allemagne","Belgique","Espagne","Finlande","France(FêteMusique)","Grèce","Italie","Luxembourg",
    "Malte","Monaco","Pays-Bas","Portugal","Saint-Marin","Slovaquie","Slovénie","Vatican"};
    //2012
    String[] m2012 = {"Allemagne","Belgique","Espagne","Finlande","France(AbbePierre)","Italie","Luxembourg","GrandsDucsLuxembourg",
    "Monaco","Portugal","Vatican","ZoneEuro"};
    //2013
    String[] m2013 = {"Pays-Bas","Belgique(100AnsMeteorologique)","Grèce(AcademiePlaton)","Finlande(Nobel)","Grèce",
    "Luxembourg(AntenneNationale)","Saint-Marin(Pinturicchio)","Finlande","Monaco","Vatican","Pays-Bas",
    "Italie(verdi)","France(Coubertin)","Vatican(Rio)","Italie","Portugal","Allemagne","Malte",
    "Slovénie","Slovaquie","Espagne","France(TraitéÉlysée)","Allemagne(TraitéÉlysée)"};
    //2014
    String[] m2014 = {"Allemagne","Belgique(PremièreGuerreMondiale)","Belgique(CroixRouge)",
    "Espagne(Gaudi)","Espagne(ChangementDeTrone)","Finlande(ToveMarikaJansson)","Finlande(Tapiovaara)",
    "France(Sida)","France(DDay)","Grèce(DoménikosTheotokopoulos)","Grèce(ÎlesIoniennes)",
    "Italie(Galileo)","Italie(Carabiniers)","Lettonie","Luxembourg(Indépendance)",
    "Luxembourg(GrandDucJean)","Malte(Indépendance)","Malte(Police)","Pays-Bas(DoublePortrait)",
    "Portugal(RévolutionDesOeillets)","Portugal(AgricultureFamiliale)","Slovaquie(EntréeUE)",
    "Slovénie(BarbaraCeljska)","Saint-Marin(GiacomoPuccini)","Saint-Marin(DonatoBramante)","Vatican(MurDeBerlin)"};
    //2015
    String[] m2015 = {"Allemagne(DrapeauEuropéen)","Allemagne(Hessen)","Allemagne(Réunification)","Andorre(Majorité)",
    "Andorre(ConseilEurope)","Andorre(UnionDouanière)","Autriche(DrapeauEuropéen)","Belgique(DrapeauEuropéen)",
    "Belgique(Développement)","Chypre(DrapeauEuropéen)","Espagne(Altamira)","Espagne(DrapeauEuropéen)",
    "Estonie(DrapeauEuropéen)","Finlande(AkseliGallenKallela)","Finlande(JeanSibelius)","Finlande(DrapeauEuropéen)",
    "France(DrapeauEuropéen)","France(PaixEurope)","France(FêteDeLaFédération)","Grèce(SpyridonLouis)",
    "Grèce(DrapeauEuropéen)","Irlande(DrapeauEuropéen)","Italie(DrapeauEuropéen)","Italie(DanteAlighieri)",
    "Italie(Milan)","Lettonie(DrapeauEuropéen)","Lettonie(PrésidenceEuropéenne)","Lettonie(Cigogne)",
    "Lituanie(DrapeauEuropéen)","Lituanie(LangueLituanienne)","Luxembourg(DrapeauEuropéen)",
    "Luxembourg(DynastieNassauWeilburg)","Luxembourg(GrandDucHenri)","Malte(DrapeauEuropéen)",
    "Malte(ProclamationRépublique)","Malte(PremierVol)","Monaco(ChateauSurLeRocher)","Pays-Bas(DrapeauEuropéen)",
    "Portugal(DrapeauEuropéen)","Portugal(Timor)","Portugal(CroixRouge)","Saint-Marin(RéunificationAllemande)",
    "Saint-Marin(DanteAlighieri)","Slovaquie(LudovitStur)","Slovaquie(DrapeauEuropéen)","Slovénie(Emona)",
    "Slovénie(DrapeauEuropéen)","Vatican(RencontreFamillePhiladelphie)"};
    //2016
    String[] m2016 = {"Allemagne(ZwingerDeDresde)","Andorre(RadioEtTélévisionPublique)","Andorre(NouvelleRéforme)",
    "Autriche(BanqueNationale)","Belgique(JeuxOlympiques)","Belgique(Organisation'ChildFocus')",
    "Espagne(AqueducDeSégovie)","Estonie(PaulKeres)","Finlande(GeorgHenrikVonWright)","Finlande(EinoLeino)",
    "France(FrançoisMitterrand)","France(UEFA)","Grèce(HolocausteMonastèreArkadi)","Grèce(DimitriMitropoulos)",
    "Irlande(Pâques)","Italie(Donatello)","Italie(Plauto)","Lettonie(IndustrieLaitière)","Lettonie(Vidzeme)",
    "Lituanie(CultureBaltiqueLietuva)","Luxembourg(GrandeDuchesseCharlotte)","Malte(Amour)","Malte(Ggantija)",
    "Monaco(MonteCarlo)","Portugal(JeuxOlympiques)","Portugal(Pont25Avril)","Saint-Marin(WilliamShakespeare)",
    "Saint-Marin(Donatello)","Slovaquie(PrésidenceEuropéenne)","Slovénie(IndépendanceSlovénie)","Vatican(Miséricorde)",
    "Vatican(GendarmerieVatican)"};
    //2017
    String[] m2017 = {"Allemagne(PortaNigraATrèves)","Andorre(HymneNational)","Andorre(Pyrénées)","Belgique(UniversitéDeLiège)",
    "Belgique(UniversitéDeGand)","Chypre(CulturePAFOS)","Espagne(SantaMariaDeNaranco)","Estonie(Indépendance)",
    "Finlande(Indépendance)","Finlande(NatureFinlandaise)","France(AugusteRodin)","France(CancerDuSein)",
    "Grèce(NikosKazantzakis)","Grèce(SiteArchéologiquePhilippes)","Italie(BasiliqueDeSanMarcoAVenise)","Italie(TitoLivio)",
    "Lettonie(Latgale)","Lettonie(Kurzeme)","Lituanie(Vilnius)","Luxembourg(ServiceMilitaireVolontaire)",
    "Luxembourg(GrandDucGuillaumeIII)","Malte(TempleHagarQim)","Malte(Paix)","Monaco(Carabiniers)",
    "Portugal(PoliceSécuritéPublique)","Portugal(RaulBrandao)","Saint-Marin(Giotto)",
    "Saint-Marin(TourismeDéveloppementDurable)","Slovaquie(UniversitéIstropolitana)","Slovénie(EuroEnSlovénie)",
    "Slovénie(DéclarationDeMai)","Vatican(ApparitionsDeFatima)","Vatican(Saint-Pierre/Saint-Paul)"};
    //2018
    String[] m2018 = {
    "Allemagne(100ansHelmutSchmidt)","Allemagne(ÉtatBerlinChâteauCharlottenburg)","Andorre(25ansConstitution)",
    "Autriche(100ansRépubliqueD'Autriche)","Espagne(Saint-Jacques-de-Compostelle)","Espagne(50ansRoiFelipeVI)",
    "Estonie(100ansRépubliqueEstonie)","Estonie(CentenaireFondationÉtatsBaltesIndépendants)","France(BleuetFrance)",
    "France(SimoneVeil)","Grèce(75ansMortKostísPalamás)","Grèce(70ansUnionDodécanèses)","Italie(60ansMinistèreSantéItalien)",
    "Italie(70ansConstitutionItalienne)","Lettonie(RégionZemgale)","Lettonie(CentenaireFondationÉtatsBaltesIndépendants)",
    "Lituanie(CentenaireFondationÉtatsBaltesIndépendants)","Lituanie(FestivalChansonEtDanse)","Luxembourg(150ansConstitutionGrandDuché)",
    "Malte(EnfantsSolidaritéNatureEtEnvironnement)","Malte(TemplesMnajdra)","Portugal(250ansImprimerieNationale)",
    "Portugal(250ansJardinsBotaniquesAjuda)","Saint-Marin(500ansNaissanceTintoret)","Saint-Marin(420ansNaissanceGianLorenzoBernini)",
    "Slovaquie(25ansRépubliqueSlovaque)","Slovénie(JournéeMondialeAbeilles)"};
    String pays;
    String from;
    String[] val;
    HashMap<String,String[]> hashMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compte);
        hashMap.put("2004",m2004);
        hashMap.put("2005",m2005);
        hashMap.put("2006",m2006);
        hashMap.put("2007",m2007);
        hashMap.put("2008",m2008);
        hashMap.put("2009",m2009);
        hashMap.put("2010",m2010);
        hashMap.put("2011",m2011);
        hashMap.put("2012",m2012);
        hashMap.put("2013",m2013);
        hashMap.put("2014",m2014);
        hashMap.put("2015",m2015);
        hashMap.put("2016",m2016);
        hashMap.put("2017",m2017);
        hashMap.put("2018",m2018);
        pays = getIntent().getStringExtra("Choix");
        from = getIntent().getStringExtra("From");
        System.out.println(from + " this that shit");
        if(from != null) {
            System.out.println(pays);
            if (from.equals("Commemo")) {
                val = hashMap.get(pays);
            } else {
                val = valeurs;
            }
        }
        try {
            datas = new JSONArray(read());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(datas==null){
            init(val);
        }

        TextView mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText(pays);
        final ListView mCompteListView = (ListView) findViewById(R.id.listViewPieces);
        ListViewPiecesAdapter adapter = new ListViewPiecesAdapter(this, val, read());
        mCompteListView.setAdapter(adapter);

        mCompteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub;
                CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox);
                cb.setChecked(!cb.isChecked());
                try {
                    datas.put(position, !datas.getBoolean(position));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println(String.valueOf(datas));
                write(String.valueOf(datas));

            }
        });
    }

    public void write(String text) {
        try {
            FileOutputStream fileOutputStream = openFileOutput(pays+".json",MODE_PRIVATE);
            fileOutputStream.write(text.getBytes());
            fileOutputStream.close();
            //Toast.makeText(getApplicationContext(),"Text Saved", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read() {
        try {
            FileInputStream fileInputStream= openFileInput(pays+".json");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            String lines;
            while ((lines=bufferedReader.readLine())!=null) {
                stringBuffer.append(lines+"\n");
            }
            System.out.println(String.valueOf(stringBuffer));
            return String.valueOf(stringBuffer);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("erreur");
        return "err";
    }

    public void init(String[] a){
        datas = new JSONArray();

        for(int i = 0; i<a.length;i++){
            System.out.println(a[i]);
            try {
                datas.put(i,false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Initialized");
        write(String.valueOf(datas));
    }
}