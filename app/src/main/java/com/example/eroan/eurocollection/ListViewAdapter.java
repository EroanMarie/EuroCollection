package com.example.eroan.eurocollection;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Eroan on 11/01/2018.
 */

public class ListViewAdapter extends ArrayAdapter {

        private final Activity context;
        private final String[] itemname;
        private final Integer[] imgid;

        public ListViewAdapter(Activity context, String[] itemname, Integer[] imgid) {
            super(context, R.layout.rowlayout, itemname);
            // TODO Auto-generated constructor stub

            this.context=context;
            this.itemname=itemname;
            this.imgid=imgid;
        }

        public View getView(int position,View view,ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.rowlayout, null,true);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.label);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

            txtTitle.setText(itemname[position]);
            imageView.setImageResource(imgid[position]);
            return rowView;

        }
    }


