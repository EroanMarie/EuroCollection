package com.example.eroan.eurocollection;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Eroan on 11/01/2018.
 */

public class ListViewPiecesAdapter extends ArrayAdapter {
    private final Activity context;
    private final String[] itemname;
    private JSONArray infos = null;
    public ListViewPiecesAdapter(Activity context, String[] itemname, String infos) {
        super(context, R.layout.piecelayout, itemname);
        // TODO Auto-generated constructor stub
        this.context=context;
        this.itemname=itemname;
        try {
            this.infos = new JSONArray(infos);
            System.out.println(this.infos.getBoolean(0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.piecelayout, null,true);
        CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
        try {
            if(infos.getBoolean(position)){
                checkBox.setChecked(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        checkBox.setText(String.valueOf(itemname[position]));
        return rowView;
    }
}