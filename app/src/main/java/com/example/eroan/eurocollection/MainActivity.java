package com.example.eroan.eurocollection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.eroan.eurocollection.R.id;
import com.example.eroan.eurocollection.R.layout;

public class MainActivity extends AppCompatActivity {

    private String[] listePaysPieces = {"Allemagne","Andorre","Autriche","Belgique","Chypre","Commémoratives","Espagne","Estonie","Finlande",
            "France","Grèce","Irlande","Italie","Lettonie","Lituanie","Luxembourg","Malte","Monaco","Pays-Bas","Portugal",
            "Slovaquie","Slovénie","Saint-Marin","Vatican"};

    private Integer[] drapeaux = {
            R.drawable.allemagne,R.drawable.andorre,R.drawable.autriche,R.drawable.belgique,R.drawable.chypre,
            R.drawable.commemoratives,R.drawable.espagne,R.drawable.estonie,R.drawable.finlande,R.drawable.france,R.drawable.grece,
            R.drawable.irlande,R.drawable.italie,R.drawable.letonnie,R.drawable.lituanie,R.drawable.luxembourg,
            R.drawable.malte,R.drawable.monaco,R.drawable.paysbas,R.drawable.portugal,R.drawable.slovaquie,
            R.drawable.slovenie,R.drawable.saintmarin,R.drawable.vatican};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);
        ListView mListView = (ListView) findViewById(id.listView);
        ListViewAdapter adapter = new ListViewAdapter(this,listePaysPieces,drapeaux);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,View view,
                                    int position,long id) {
                // TODO Auto-generated method stub
                String selectedItem = listePaysPieces[+position];
                System.out.println(selectedItem);
                Toast.makeText(getApplicationContext(),selectedItem,Toast.LENGTH_SHORT);
                if(selectedItem=="Commémoratives"){
                    Intent intent = new Intent(MainActivity.this,CommemoActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MainActivity.this,CompteActivity.class);
                    intent.putExtra("Choix",selectedItem);
                    intent.putExtra("From",selectedItem);
                    startActivity(intent);
                }
            }
        });
    }
}
